<pre>
<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

require '../GoodNewsApi/ApiMethods.php';

$oauth_consumer_key = '48C18E20-4EEF-4A70-9F31-16D27D360E6E';
$oauth_consumer_secret = 'AC1BFB54-3753-4C18-9864-02B6DD549B06';

$api = new ApiMethods($oauth_consumer_key, $oauth_consumer_secret);

$api->setTargetUrl('https://www.urlshortener.best/site/pricing');
$api->setRefNote('Test of Good News API');
$api->setCodeKey('pricing');
$api->setShownNote('This message will be displayed to users on click through.');
$api->setUsePreview(true);
$api->setCreateQrCode(true);
$api->setQrColor('#dd0000');
$api->setQrSize(150);
$api->setCustomDomain('new.vg');
$api->setExpirationDate('2021-05-01');
$response = $api->create();
print_r($response);


