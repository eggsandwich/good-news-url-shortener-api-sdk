# Good News URL Shortener API SDK

A PHP SDK for interfacing with the Good News URL Shortener.

## Installation

Simply clone the repo and install the GoodNewsApi directory in your directory tree. Then REQUIRE ApiMethods.php file as in 

```php
require 'GoodNewsApi/ApiMethods.php';
```

## Full Documentation

Find full documentation of the API endpoints at:
[www.urlshortenerapi.com](https://www.urlshortenerapi.com)

## License
[MIT](https://choosealicense.com/licenses/mit/)

