<?php

require "ApiConnect.php";
require "OauthSign.php";

/**
 * Class Curl
 */
class Curl extends ApiConnect
{
    private $goodNewsApiDomain = 'https://api.goodnews.best';
    private $version = 'apiv1';
    private $controller = 'good-news';

    /**
     * @param $ver
     */
    public function setApiVersion($ver){
       $this->version = $ver;
    }

    /**
     * @param $action
     * @param $params
     * @return mixed
     * @throws Exception
     * Connect with Good News API
     */
    protected function doCurl($action, $params){

        $postfields = [];

        foreach ($params as $param){
            $postfields[$param] = $this->$param;
        }

        // Set oauth params
        $postfields['oauth_consumer_key'] = $this->consumer_key;
        $postfields['oauth_nonce'] = uniqid();
        $postfields['oauth_signature_method'] = 'HMAC-SHA1';
        $postfields['oauth_version'] = '1.0';
        $postfields['oauth_timestamp'] = time();
        $url = $this->goodNewsApiDomain.'/'.$this->version.'/'.$this->controller.'/'.$action;

        $postfields['oauth_signature'] = OauthSign::getOauthSignature($postfields, $url, $this->consumer_secret);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 15);
        $raw_response = trim(curl_exec($curl));
        if(curl_errno($curl)) {
            $error_message = curl_error($curl);
            throw new Exception('Curl Error: '.$error_message);
        }
        curl_close($curl);

        return json_decode($raw_response);
    }
}