<?php

/**
 * Class OauthSign
 */
class OauthSign
{
    /**
     * @param $params
     * @param $url
     * @param $oauth_secret
     * @return string
     */
    public static function getOauthSignature($params, $url, $oauth_secret)
    {
        if (array_key_exists('oauth_signature', $params)){
            unset($params['oauth_signature']);
        }

        if (array_key_exists('realm', $params)){
            unset($params['realm']);
        }

        // Parameters must be in alphabetical order when constructing the parameter string
        ksort($params);

        // Create param string for signature
        $param_str = '';
        foreach ($params as $k => $v){
            $param_str .= "{$k}=" . rawurlencode($v) ."&";
        }

        $param_str = rtrim($param_str,'&');

        // Create base string for signature creation
        $base_string = 'POST&'.rawurlencode($url).'&'.rawurlencode($param_str);

        // Add & to end of secret per spec
        $secret_str = $oauth_secret.'&';

        // Create signature hash using sha1, base string and secret
        $signature = hash_hmac( 'sha1', $base_string, $secret_str, true );

        return base64_encode($signature);
    }
}