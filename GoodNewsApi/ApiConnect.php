<?php

/**
 * Class ApiConnect
 */
class ApiConnect
{
    /*
     * @var string
     */
    protected $consumer_key;

    /*
     * @var string
     */
    protected $consumer_secret;

    /*
     * @var string
     */
    protected $target_url;

    /*
     * @var string
     */
    protected $expiration_date;

    /*
     * @var string
     */
    protected $ref_note;

    /*
     * @var string
     */
    protected $shown_note;

    /*
     * @var bool
     */
    protected $use_preview;

    /*
    * @var string
    */
    protected $code_key;

    /*
     * @var string
     */
    protected $custom_domain;

    /*
     * @var bool
     */
    protected $create_qr_code;

    /*
     * @var int
     */
    protected $qr_size;

    /*
     * @var string
     */
    protected $qr_color;

    /**
     * ApiConnect constructor.
     * @param string $consumer_key
     * @param string $consumer_secret
     */
    public function __construct($consumer_key = '', $consumer_secret = '')
    {
        // Key and secret can be set in constructor here or through setters below
        $this->consumer_key = $consumer_key;
        $this->consumer_secret = $consumer_secret;
    }

    /**
     * @param $string
     */
    public function setConsumerKey($string){
        $this->consumer_key = $string;
    }

    /**
     * @param $string
     */
    public function setConsumerSecret($string){
        $this->consumer_secret = $string;
    }

    /**
     * @param $url
     * @throws Exception
     */
    public function setTargetUrl($url){
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
            throw new Exception('Invalid URL');
        }
        $this->target_url = $url;
    }

    /**
     * @param $date
     * @throws Exception
     */
    public function setExpirationDate($date){
        if (empty($date)){
            $this->expiration_date = null;
        }
        else {
            $bad_date = false;
            if (!preg_match('/\d\d\d\d-\d\d-\d\d/', $date))
                $bad_date = true;
            if (!$bad_date) {
                list($year, $month, $day) = explode('-', $date);
                if (!checkdate($month, $day, $year))
                    $bad_date = true;
            }
            if ($bad_date)
                throw new Exception('Invalid Expiration Date.');
            $this->expiration_date = $date;
        }
    }

    /**
     * @param $string
     */
    public function setRefNote($string){
        $this->ref_note = $string;
    }

    /**
     * @param $string
     */
    public function setShownNote($string){
        $this->shown_note = $string;
    }

    /**
     * @param $bool
     */
    public function setUsePreview($bool){
        $this->use_preview = empty($bool) ? 0 : 1;
    }

    /**
     * @param $string
     * @throws Exception
     */
    public function setCodeKey($string){
        if (!preg_match('/^[a-z0-9-]+$/i', $string))
            throw new Exception('Invalid Code Key.');
        $this->code_key = $string;
    }

    /**
     * @param $domain
     * @throws Exception
     */
    public function setCustomDomain($domain){
        if (preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $domain) //valid chars check
            && preg_match("/^.{1,253}$/", $domain) //overall length check
            && preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $domain) //length of each label
        ){
            $this->custom_domain = $domain;
        }
        else throw new Exception('Invalid Domain Name.');
    }

    /**
     * @param $bool
     */
    public function setCreateQrCode($bool){
        $this->create_qr_code = empty($bool) ? 0 : 1;
    }

    /**
     * @param $hex
     * Set QR color using HTML color hexadecimal notation with leading #
     * @throws Exception
     */
    public function setQrColor($hex){
        if (!preg_match('/#[0-9a-f]{6}/',$hex))
            throw new Exception('QR Code Color Invalid Hex Value.');
        $this->qr_color = $hex;
    }

    /**
     * @param $int
     * @throws Exception
     */
    public function setQrSize($int){
        if (!is_numeric($int))
            throw new Exception('QR Code must be number.');
        $this->qr_size = $int;
    }

}