<?php

require "Curl.php";

/**
 * Class ApiMethods
 */
class ApiMethods extends Curl
{
    /**
     * @return object
     * @throws Exception
     * Returns status, error, short URL, code key, QR code URL
     */
    public function create(){
        $params = [
            'target_url',
            'expiration_date',
            'ref_note',
            'shown_note',
            'use_preview',
            'code_key',
            'custom_domain',
            'create_qr_code',
            'qr_size',
            'qr_color'
        ];

        return $this->doCurl('create', $params);
    }

    /**
     * @return object
     * @throws Exception
     * Returns status, error, QR code URL
     */
    public function getQrCode(){
        $params = [
            'code_key',
            'custom_domain',
            'qr_size',
            'qr_color'
        ];

        return $this->doCurl('getqr', $params);
    }

    /**
     * @return object
     * @throws Exception
     * Returns list of custom domains
     */
    public function listDomains(){
        $params = [
        ];

        return $this->doCurl('list-domains', $params);
    }

    /**
     * @return object
     * @throws Exception
     * Enable short url
     * return status, error
     */
    public function enable(){
        $params = [
            'code_key',
        ];

        return $this->doCurl('enable', $params);
    }

    /**
     * @return object
     * @throws Exception
     * Disable short URL
     * Returns status, error
     */
    public function disable(){
        $params = [
            'code_key',
        ];

        return $this->doCurl('disable', $params);
    }

    /**
     * @return object
     * @throws Exception
     * Returns status, error, code key
     */
    public function update(){
        $params = [
            'target_url',
            'expiration_date',
            'ref_note',
            'shown_note',
            'use_preview',
            'code_key',
        ];

        return $this->doCurl('update', $params);
    }

}